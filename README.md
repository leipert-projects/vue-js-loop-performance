# vue-js-loop-performance

This repo contains code to run a bench mark to show the performance improvements vue loops without keys, index keys and proper keys.

## Vue with no key

```vue
<div class="container" id="app-no-key">
  <ul>
    <user v-for="(approver) in approvers" v-bind:user="approver.user"/>
  </ul>
</div>
```

## Vue with index key

```vue
<div class="container" id="app-index-key">
  <ul>
    <user v-for="(approver, index) in approvers" v-bind:key="index" v-bind:user="approver.user"/>
  </ul>
</div>
```

## Vue with proper key

```vue
<div class="container" id="app-proper-key">
  <ul>
    <user v-for="(approver) in approvers" v-bind:key="approver.user.id" v-bind:user="approver.user"/>
  </ul>
</div>
```

## Results 

Example

```
Vue without key x 9,878 ops/sec ±2.53% (77 runs sampled)
Vue with index key x 9,318 ops/sec ±2.79% (76 runs sampled)
Vue with proper key x 9,834 ops/sec ±3.82% (75 runs sampled)
Fastest is Vue without key,Vue with proper key
Vue without key x 9,426 ops/sec ±1.79% (77 runs sampled)
Vue with index key x 9,477 ops/sec ±0.70% (81 runs sampled)
Vue with proper key x 10,108 ops/sec ±1.53% (78 runs sampled)
Fastest is Vue with proper key
Vue without key x 9,414 ops/sec ±1.92% (82 runs sampled)
Vue with index key x 9,287 ops/sec ±0.99% (81 runs sampled)
Vue with proper key x 10,261 ops/sec ±2.09% (81 runs sampled)
Fastest is Vue with proper key
Vue without key x 9,896 ops/sec ±1.75% (78 runs sampled)
Vue with index key x 9,564 ops/sec ±0.89% (79 runs sampled)
Vue with proper key x 10,907 ops/sec ±0.91% (81 runs sampled)
Fastest is Vue with proper key
Vue without key x 9,230 ops/sec ±2.92% (77 runs sampled)
Vue with index key x 9,336 ops/sec ±2.87% (77 runs sampled)
Vue with proper key x 10,540 ops/sec ±1.16% (77 runs sampled)
Fastest is Vue with proper key
```