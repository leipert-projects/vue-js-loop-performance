var Benchmark = require('benchmark');
require('jsdom-global')()

var suite = new Benchmark.Suite;
var Vue = require('vue');
var { shallowMount: mount } = require('@vue/test-utils');

const approvers = [];

const length = 10;

for (let i = 0; i < length; i++){
   approvers.push({user: {id: i, name: 'User ' + (length - i), rand: Math.random() }});
}

const shared = {
  data: function() {
    return {
      approvers: [...approvers]
    };
  }
};

Vue.component("user", {
    props: ["user"],
    template: "<li>{{ user.name }} - {{ user.id }} = {{ user.rand }}</li>"
  });

var noKey = mount({ ...shared, template: `
<div class="container" id="app-no-key">
  <ul>
    <user v-for="(approver) in approvers" v-bind:user="approver.user"/>
  </ul>
</div>
`});

var indexKey = mount({ ...shared, template: `
<div class="container" id="app-index-key">
  <ul>
    <user v-for="(approver, index) in approvers" v-bind:key="index" v-bind:user="approver.user"/>
  </ul>
</div>
`});


var properKey = mount({ ...shared, template: `
<div class="container" id="app-proper-key">
  <ul>
    <user v-for="(approver) in approvers" v-bind:key="approver.user.id" v-bind:user="approver.user"/>
  </ul>
</div>
`});

// add tests
suite
.add('Vue without key',  {
    defer: true,
    fn: function(deferred) {
        noKey.vm.approvers.reverse();
        return Vue.nextTick()
            .then(() => deferred.resolve())
            .catch((err)=> {console.warn(err)});
    }
  })
  .add('Vue with index key',  {
    defer: true,
    fn: function(deferred) {
        indexKey.vm.approvers.reverse();
        return Vue.nextTick()
            .then(() => deferred.resolve())
            .catch((err)=> {console.warn(err)});
    }
  })
  .add('Vue with proper key',  {
    defer: true,
    fn: function(deferred) {
        properKey.vm.approvers.reverse();
        return Vue.nextTick()
            .then(() => deferred.resolve())
            .catch((err)=> {console.warn(err)});
    }
  })
.on('cycle', function(event) {
  console.log(String(event.target));
})
.on('complete', function() {
  console.log('Fastest is ' + this.filter('fastest').map('name'));
})
// run async
.run({ 'async': false });
